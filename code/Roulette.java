import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel roulette = new RouletteWheel();
        boolean condition = true;
        int totalFortune = 1000;
        while (play()) {
            roulette.spin();
            int bet = promptBet();

            if (checkWin(roulette.getValue(), promptSpot())) {
                totalFortune += bet * 35;
            } else {
                totalFortune -= bet;
            }
            System.out.println("The spot was: " + roulette.getValue());
            System.out.println("Your total fortune is: " + totalFortune);
        }

    }

    /*
     * public static boolean bet(String prompt){
     * prompt=prompt.toLowerCase();
     * if(prompt.equals("yes")){
     * return true;
     * }else{
     * return false;
     * }
     * }
     */
    public static boolean play() {
        System.out.println("Would you like to make ba bet(yes/no): ");
        Scanner reader = new Scanner(System.in);
        String prompt = reader.nextLine();
        prompt = prompt.toLowerCase();
        if (prompt.equals("yes")) {
            return true;
        } else {
            return false;
        }
    }

    public static int promptSpot() {
        System.out.println("Enter the spot where you want to place your bet on: ");
        Scanner reader = new Scanner(System.in);
        int spot = reader.nextInt();
        return spot;
    }

    public static int promptBet() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a bet: ");
        int bet = reader.nextInt();
        return bet;
    }

    public static boolean checkWin(int realSpot, int playerSpot) {
        if (realSpot == playerSpot) {
            return true;
        } else {
            return false;
        }
    }
}
