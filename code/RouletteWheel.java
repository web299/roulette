import java.util.Random;
public class RouletteWheel{
  private Random randomNum;
  private int lastSpin;
  public RouletteWheel(){
      this.randomNum=new Random();
  }
  public void spin(){
      this.lastSpin=randomNum.nextInt(37);
  }
  public int getValue(){
      return this.lastSpin;
  }
}
